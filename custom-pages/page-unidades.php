<?php
/*
    Template Name: Lista-Unidades
*/
?>

<?php get_header(); ?>

<section class="topo topo-internas">
    <header class="d-flex flex-md-row flex-column">
        <div class="call-menu">
            <div class="sanduiche">
                <?php get_template_part('svg/ico', 'menu'); ?>
            </div>
            <?php get_template_part('menu/menu', 'principal'); ?>
        </div>
        <div class="logo-ifly">
            <a href="/">
                <h1>
                    <?php get_template_part('svg/logo', 'ifly'); ?>
                </h1>
            </a>
        </div>
        <div class="call-buy ml-auto text-center">
            <a href="<?php echo get_option('buy_tickets_url'); ?>">
                Compre agora
                <?php get_template_part('svg/ico', 'calendar'); ?>
            </a>
        </div>
    </header>

</section>

<section class="box-interna">
    <div class="container lista-unidades">

        <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                the_title($before = "<h2>", $after = "</h2>");
            }
        }

        $unidades = new WP_Query(array(
            'post_type' => 'unidades',
            'order' => 'ASC'
        ));

        if ($unidades->have_posts()) {
            while ($unidades->have_posts()) {
                $unidades->the_post();
                get_template_part('parts/unidade', 'detalhe');
            }
        } else {
            // no posts found
        }
        wp_reset_postdata();
        ?>
    </div>
</section>

<?php get_footer(); ?>