<?php
/*
    Template Name: Home
*/
?>
<?php get_header(); ?>

<div id="home">

    <section class="topo">
        <header class="d-flex flex-md-row flex-column">
            <div class="call-menu">
                <div class="sanduiche">
                    <?php get_template_part('svg/ico', 'menu'); ?>
                </div>
                <?php get_template_part('menu/menu', 'principal'); ?>
            </div>
            <div class="logo-ifly">
                <a href="/">
                    <h1>
                        <?php get_template_part('svg/logo', 'ifly'); ?>
                    </h1>
                </a>
            </div>
            <div class="call-buy ml-auto text-center">
                <a target="_blank" href="<?php echo get_option('buy_tickets_url'); ?>">
                    Compre agora <?php get_template_part('svg/ico', 'calendar'); ?>
                </a>
            </div>
        </header>
        <?php get_template_part('menu/menu', 'social-icon-lateral'); ?>

        <div id="carousel-ifly" class="carousel slide" data-ride="carousel" data-touch="true" data-interval=<?php echo get_option('carousel_timer'); ?>>
            <div class="carousel-inner">
                <?php
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        the_content();
                    }
                }
                ?>
            </div>
            <a class="carousel-control-next" href="#carousel-ifly" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <a class="carousel-control-prev" href="#carousel-ifly" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
        </div>

    </section>

    <section class="hashtag">
        <span><?php echo get_option('text_before_hashtag'); ?></span>

        <a href="">#<?php echo get_option('hashtag_campaign'); ?></a></section>

    <section class="ifly" style="background: url(<?php echo get_template_directory_uri() . '/' ?>img/bg-ifly.jpg) no-repeat top; background-size:cover;">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-12">
                    <h2>A iFLY</h2>
                </div>
                <div class="col-md-12">
                    <p>
                        A iFLY é a realização de um dos sonhos mais antigos da humanidade. Somos uma escola de paraquedismo indoor, que ensina as técnicas de voo e manobras da modalidade em um túnel
                        de vento de alta tecnologia. Voar com a iFLY é o primeiro passo para se divertir e encontrar novos desafios.
                    </p>
                    <p>
                        <ul class="d-flex flex-column flex-md-row justify-content-between">
                            <li>A partir dos 3 anos, até 103 anos</li>
                            <li>Voo 100% seguro</li>
                            <li>Equipamento de segurança</li>
                            <li>Professores qualificados</li>
                        </ul>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="modalidades">
        <div class="container">
            <div class="row">
                <?php

                $fields = get_fields(get_the_ID());

                foreach ($fields['lista_de_categorias'] as $id) {
                    $unidade_page = get_post($id);
                    $unidade_fields = get_fields($id);

                    set_query_var('title', $unidade_page->post_title);
                    set_query_var('url', $unidade_page->post_name);
                    set_query_var('url-img', get_the_post_thumbnail_url($id, 'full'));
                    set_query_var('descricao', $unidade_fields['descricao_destaque']);

                    // print_r($unidade_page);

                    echo get_template_part('parts/categoria', 'card');
                }

                // print_r($fields['lista_de_categorias']);

                wp_reset_postdata();
                ?>
            </div>
        </div>
    </section>

    <section class="equipe">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Equipe</h2>
                </div>
                <div class="col-md-12 mb-5">
                    <p>
                        Nossa equipe AMA voar! Todos têm verdadeira paixão pelo paraquedismo indoor. E você aprende a voar com quem entende do assunto: todos os nossos professores são paraquedistas
                        profissionais, com muitos saltos no currículo.
                    </p>
                </div>
            </div>
            <div class="row d-flex flex-row flex-md-row justify-content-center">

                <?php

                $unidades = new WP_Query(array(
                    'post_type' => 'instrutor',
                    'order' => 'ASC'
                ));

                $count = $unidades->found_posts;

                if ($unidades->have_posts()) {
                    while ($unidades->have_posts()) {
                        $unidades->the_post();

                        get_template_part('parts/instrutor', 'card');
                    }
                } else {
                    // no posts found
                }

                wp_reset_postdata();
                ?>

            </div>
        </div>
    </section>

    <section class="tripadvisor">
        <div class="container">
            <h2>Quem Voou Indica</h2>
            <?php
            dynamic_sidebar('tripadvisor_area');
            ?>
        </div>
    </section>

    <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>

    <section class="compra ifly" style="background: url(<?php echo get_the_post_thumbnail_url($post_id, 'full'); ?>) no-repeat top center;">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-12">
                    <h2>GARANTA SEU VOO!</h2>
                    <p>Descubra porque voar é só o começo. Compre seu voo e viva uma experiência única.</p>
                </div>
            </div>
        </div>
        <a target="_blank" href="<?php echo get_option('buy_tickets_url'); ?>" class="btn-compra-box">
            Compre e agende agora
            <?php get_template_part('svg/ico', 'calendar'); ?>
        </a>
    </section>

    <?php endwhile; ?>
    <?php endif; ?>

    <section class="instagram">
        <div class="container mt-5">
            <h2>Instagram</h2>
            <?php dynamic_sidebar('instagram_area');
            ?>
        </div>
    </section>

    <section class="unidades">
        <div class="container mt-5">
            <h2>Nossas Escolas</h2>
            <div class="row d-flex justify-content-center">

                <?php

                $unidades = new WP_Query(array(
                    'post_type' => 'unidades',
                    'order' => 'ASC'
                ));

                $count = $unidades->found_posts;

                if ($unidades->have_posts()) {
                    while ($unidades->have_posts()) {
                        $unidades->the_post();
                        if ($unidades->current_post < 3) {
                            get_template_part('parts/unidade', 'card');
                        }
                    }
                } else {
                    // no posts found
                }

                wp_reset_postdata();
                ?>

            </div>

            <?php if ($count > 3) : ?>
            <div class="row ver-todos">
                <a href="#">Ver Todos >>></a>
            </div>
            <?php endif; ?>

        </div>
    </section>


    <?php get_footer(); ?>