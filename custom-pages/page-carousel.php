<?php
/*
    Template Name: Video Carousel
*/
//test
?>
<?php get_header(); ?>

<div id="home">

    <section class="topo">
        <header class="d-flex flex-md-row flex-column">
            <div class="call-menu">
                <div class="sanduiche">
                    <?php get_template_part('svg/ico', 'menu'); ?>
                </div>
                <?php get_template_part('menu/menu', 'principal'); ?>
            </div>
            <div class="logo-ifly">
                <a href="/">
                    <h1>
                        <?php get_template_part('svg/logo', 'ifly'); ?>
                    </h1>
                </a>
            </div>
            <div class="call-buy ml-auto text-center">
                <a href="<?php echo get_option('buy_tickets_url'); ?>">
                    Compre agora
                    <?php get_template_part('svg/ico', 'calendar'); ?>
                </a>
            </div>
        </header>
        <?php get_template_part('menu/menu', 'social-icon-lateral'); ?>

        <div id="carousel-ifly" class="carousel slide" data-ride="carousel" data-touch="true" data-interval=5000>
            <div class="carousel-inner">
                <?php
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        the_content();
                    }
                }
                ?>
            </div>
            <a class="carousel-control-next" href="#carousel-ifly" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <a class="carousel-control-prev" href="#carousel-ifly" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
        </div>

    </section>

    <section class="hashtag"><a href="">#eupossovoar</a></section>

    <section class="modalidades">
        <div class="container">
            <div class="row">

                <div class="col-md-4 col-12">
                    <div class="modalidade-ifly">
                        <a href="" class="modadalidade-chamada d-flex flex-column">
                            <span class="modalidade-tit">Iniciantes</span>
                            <p>Descubra a incrível sensação únida do Paraquedismo Indoor</p>
                            <span class="modalidade-seta ml-auto">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                    <path id="ic_arrow_forward_24px"
                                        d="M12,4,10.59,5.41,16.17,11H4v2H16.17l-5.58,5.59L12,20l8-8Z"
                                        transform="translate(-4 -4)" />
                                </svg>
                            </span>
                        </a>
                        <div class="modalidade-img"><img
                                src="<?php echo get_template_directory_uri() . '/' ?>img/item1.png" class="img-fluid">
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-12">
                    <div class="modalidade-ifly">
                        <a href="" class="modadalidade-chamada d-flex flex-column">
                            <span class="modalidade-tit">Eventos</span>
                            <p>Organize seu evento em um lugar único</p>
                            <span class="modalidade-seta ml-auto">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                    <path id="ic_arrow_forward_24px"
                                        d="M12,4,10.59,5.41,16.17,11H4v2H16.17l-5.58,5.59L12,20l8-8Z"
                                        transform="translate(-4 -4)" />
                                </svg>
                            </span>
                        </a>
                        <div class="modalidade-img"><img
                                src="<?php echo get_template_directory_uri() . '/' ?>img/item2.png" class="img-fluid">
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-12">
                    <div class="modalidade-ifly">
                        <a href="" class="modadalidade-chamada d-flex flex-column">
                            <span class="modalidade-tit">Profissionais</span>
                            <p>Lorem ipsum set demad et loris clusun van dar in lor grav</p>
                            <span class="modalidade-seta ml-auto">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                    <path id="ic_arrow_forward_24px"
                                        d="M12,4,10.59,5.41,16.17,11H4v2H16.17l-5.58,5.59L12,20l8-8Z"
                                        transform="translate(-4 -4)" />
                                </svg>
                            </span>
                        </a>
                        <div class="modalidade-img"><img
                                src="<?php echo get_template_directory_uri() . '/' ?>img/item3.png" class="img-fluid">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>

    <section class="compra"
        style="background: url(<?php echo get_the_post_thumbnail_url($post_id, 'full'); ?>) no-repeat center;">
        <a href="<?php echo get_option('buy_tickets_url'); ?>" class="btn-compra-box">
            Compre e agende agora
            <?php get_template_part('svg/ico', 'calendar'); ?>
        </a>
    </section>

    <?php endwhile; ?>
    <?php endif; ?>

    <section class="unidades">

        <div class="container">
            <div class="row d-flex justify-content-center">

                <?php

                $unidades = new WP_Query(array(
                    'post_type' => 'unidades',
                    'order' => 'ASC'
                ));

                $count = $unidades->found_posts;

                if ($unidades->have_posts()) {
                    while ($unidades->have_posts()) {
                        $unidades->the_post();
                        if ($unidades->current_post < 3) {
                            get_template_part('parts/unidade', 'card');
                        }
                    }
                } else {
                    // no posts found
                }

                wp_reset_postdata();
                ?>

            </div>

            <?php if ($count > 3) : ?>
            <div class="row ver-todos">
                <a href="#">Ver Todos >>></a>
            </div>
            <?php endif; ?>

        </div>
    </section>


    <?php get_footer(); ?>