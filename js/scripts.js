jQuery(function() {
	// ABRE O MENU PRINCIPAL
	jQuery('.sanduiche').click(function() {
		jQuery('.menu-ifly').toggleClass('menu-ativo');
		jQuery('body').addClass('body-fixed');
	});

	jQuery('.close-menu').click(function() {
		jQuery('.menu-ifly').toggleClass('menu-ativo');
		jQuery('body').removeClass('body-fixed');
	});
	
	// Abre submenu

  	jQuery('.menu-ifly ul li').click(function(){ 
  		jQuery(this).stop().find( "ul" ).fadeToggle( "fast", "linear" );
  		jQuery(this).toggleClass('menu-seta');
	});


	//Barra fixa
	var shrinkHeader = 112;

	jQuery(window).scroll(function() {
		var scroll = getCurrentScroll();

		if (scroll >= shrinkHeader) {
			jQuery('header').addClass('fixed-top');
		} else {
			jQuery('header').removeClass('fixed-top');
		}
	});

	function getCurrentScroll() {
		return window.pageYOffset || document.documentElement.scrollTop;
	}
});

//sanduiche
//menu-ifly