<div class="col-md-4 col-12">
    <div class="modalidade-ifly">
        <a href="/<?= get_query_var('url') ?>/" class="modadalidade-chamada d-flex flex-column">
            <span class="modalidade-tit"><?= get_query_var('title') ?></span>
            <p><?= get_query_var('descricao') ?></p>
            <span class="modalidade-seta ml-auto">
                Conheça
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="#fff">
                    <path id="ic_arrow_forward_24px" d="M12,4,10.59,5.41,16.17,11H4v2H16.17l-5.58,5.59L12,20l8-8Z" transform="translate(-4 -4)" />
                </svg>
            </span>
        </a>
        <div class="modalidade-img"><img src="<?= get_query_var('url-img') ?>" class="img-fluid">
        </div>
    </div>
</div>