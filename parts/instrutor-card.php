<div class="col-md-2 col-6">
    <div class="membro">
        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>" class="img-fluid">
        <span><b><?php echo get_the_title(); ?></b></span>
        <span><b>IBA</b> <?php echo get_post_custom_values('iba_level', get_the_ID())[0] ?></span>
        <span><b><?php echo get_fields(get_the_ID())['unidade'][0]->post_title ?></b></span>
    </div>
</div>