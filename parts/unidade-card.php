<div class="col-md-4 col-12">
    <div class="unidade-ifly">
        <span class="unidade-tit">
            <?php get_template_part('svg/ico', 'ifly-unidades'); ?>
            <?php echo get_the_title(); ?>
        </span>
        <div class="unidade-map">
            <?php echo nl2br(get_post_custom_values('google_maps_embed', get_the_ID())[0]); ?>
        </div>
        <div class="unidade-endereco">
            <?php echo nl2br(get_post_custom_values('endereco', get_the_ID())[0]); ?>
        </div>
        <div class="unidade-redes">

            <!-- <svg id="_029-instagram" data-name="029-instagram" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                viewBox="0 0 24 24">
                <path id="Caminho_9" data-name="Caminho 9"
                    d="M16.851,0H7.15A7.158,7.158,0,0,0,0,7.15v9.7A7.158,7.158,0,0,0,7.15,24h9.7A7.158,7.158,0,0,0,24,16.851V7.15A7.158,7.158,0,0,0,16.851,0Zm4.735,16.851a4.735,4.735,0,0,1-4.735,4.735H7.15a4.735,4.735,0,0,1-4.735-4.735V7.15A4.735,4.735,0,0,1,7.15,2.414h9.7A4.735,4.735,0,0,1,21.586,7.15v9.7Z"
                    fill="#000" />
                <path id="Caminho_10" data-name="Caminho 10"
                    d="M139.207,133a6.207,6.207,0,1,0,6.207,6.207A6.214,6.214,0,0,0,139.207,133Zm0,10A3.793,3.793,0,1,1,143,139.207,3.793,3.793,0,0,1,139.207,143Z"
                    transform="translate(-127.207 -127.207)" fill="#000" />
                <circle id="Elipse_1" data-name="Elipse 1" cx="1.487" cy="1.487" r="1.487"
                    transform="translate(16.732 4.352)" fill="#000" />
            </svg> -->
        </div>
        <div class="unidade-telefone">
            <?php echo nl2br(get_post_custom_values('telefone', get_the_ID())[0]); ?>
        </div>
    </div>
</div>

<?php
//echo '<li>' . get_the_title() . '</li>';
//echo '<li>' . nl2br(get_post_custom_values('endereco', get_the_ID())[0]) . '</li>';