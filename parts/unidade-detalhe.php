<div class="card-unidade-detalhe">
    <div class="row">
        <?php the_title($before = "<h3>", $after = "</h3>"); ?>
    </div>
    <div class="row">
        <div class="col-5">
            <p>
                <?php echo nl2br(get_post_custom_values('endereco', get_the_ID())[0]); ?>
            </p>

            <p>
                <?php echo nl2br(get_post_custom_values('telefone', get_the_ID())[0]); ?>
            </p>


        </div>
        <div class="col-7 ">

        </div>
    </div>
    <div class="row ">
        <div class="col-5 embed-unidade">
            <?php echo nl2br(get_post_custom_values('google_maps_embed', get_the_ID())[0]); ?>
        </div>
        <div class="col-7 img-unidade"><img class="" src="<?php echo get_the_post_thumbnail_url(); ?>"></div>
    </div>
</div>