<section class="box-interna">
    <div class="container lista-unidades">
        <?php
        $unidades = new WP_Query(array(
            'post_type' => 'unidades',
            'order' => 'ASC'
        ));

        if ($unidades->have_posts()) {
            while ($unidades->have_posts()) {
                $unidades->the_post();
                get_template_part('parts/unidade', 'detalhe');
            }
        } else {
            // no posts found
        }
        wp_reset_postdata();
        ?>
    </div>
</section>