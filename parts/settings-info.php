<h1>Como Usar o Tema</h1>

<ul>
    <li><a href="#video-home">Alterar o Vídeo da Homepage</a></li>
    <li><a href="#cadastro-unidade">Cadastrar uma Unidade</a></li>
</ul>

<h2 id="video-home">Alterar o Vídeo da Homepage</h2>

<p>Para alterar o video da home:</p>
<ol>
    <li>Em Páginas, abra a página <b>Home</b></li>
    <li>No Menu superior, selecione a opção <b>Navegação de Blocos</b></li>
    <img src="/wp-content/themes/iflyBrasil/img-docs/video-1.png">
    <li>E em seguida a opção <b>Background (AWB)</b></li>
    <img src="/wp-content/themes/iflyBrasil/img-docs/video-2.png">
    <li>Na lateral direita será exibido um menu com as opções disponiveis para seleção de video (youtube, vimeo, ou
        envio do arquivo)</li>
    <img src="/wp-content/themes/iflyBrasil/img-docs/video-3.png">
    <li>Para substituir um arquivo já enviado, é preciso primeiro remover o arquivo atual, e em seguida submeter um novo
        arquivo .mp4</li>
</ol>

<h2 id="cadastro-unidade">Cadastrar uma Unidade</h2>

<p>Para cadasrtrar uma unidade, preencha os seguintes campos personalizados:</p>
<ol>
    <li><i>telefone</i> - Telefone da Unidade</li>
    <li><i>endereco</i> - Enredeço da Unidade</li>
    <li><i>google_maps_embed</i> - Código de Incorporação do Google Maps</li>
</ol>