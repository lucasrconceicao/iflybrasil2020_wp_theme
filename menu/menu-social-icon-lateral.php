<div class="social-ifly d-flex flex-md-column flex-row justify-content-center">
    <?php wp_nav_menu(array(
        'theme_location' => 'social_icon_menu',
        'menu_class' => 'd-flex flex-md-column flex-row justify-content-center'
    )); ?>
</div>