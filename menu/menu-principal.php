<nav class="menu-ifly">
    <div class="close-menu">
        <?php get_template_part('svg/ico', 'close-menu'); ?>
    </div>
    <?php wp_nav_menu(array('theme_location' => 'main_menu')); ?>

    <?php if (get_option('show_buy_menu')) : ?>
    <ul>
        <li class="menu-comprar call-buy menu-item menu-item-type-custom menu-item-object-custom">
            <a class="comprar" target="_blank" href="<?php echo get_option('buy_tickets_url'); ?>">
                Compre Seu Voo <?php get_template_part('svg/ico', 'calendar'); ?>
            </a>

        </li>
    </ul>
    <?php endif; ?>

</nav>