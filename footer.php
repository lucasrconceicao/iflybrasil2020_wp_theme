<?php wp_footer(); ?>

<footer>
    <div class="container">
        <?php get_template_part('svg/ifly', 'footer'); ?>

        <?php get_template_part('menu/menu', 'footer'); ?>

        <div class="contatos">
            <div class="contatos-redes">
                <?php get_template_part('menu/menu', 'social-icon-rodape'); ?>
            </div>
        </div>

    </div>

    <div class="assinatura">
        Manutenção dos mais altos padrões de segurança, treinamento e competições. <br>
        © 2020 iFLY, All Rights Reserved.
    </div>

</footer>

</div>

</body>

</html>