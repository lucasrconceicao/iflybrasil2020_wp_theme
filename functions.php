<?php

/*
    ===================================
     Register Theme Supports
    ===================================
*/
add_theme_support('menus');
add_theme_support('post-thumbnails');


/*
    ===================================
     Style and Scripts
    ===================================
*/
function load_scripts()
{
    //CSS
    // wp_enqueue_style('fonts-ifly-flama', 'https://cdn.iflyworld.com/fonts/flama/font.css', array(), null, 'all');
    // wp_enqueue_style('fonts-ifly-united', 'https://cdn.iflyworld.com/fonts/united-sans/font.css', array(), null, 'all');
    // wp_enqueue_style('fonts-ifly-united-italic', 'https://cdn.iflyworld.com/fonts/united-italic/font.css', array(), null, 'all');

    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), null, 'all');
    wp_enqueue_style('theme-css', get_template_directory_uri() . '/css/style.css', array(), null, 'all');
    wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:300,400,400i,700,700i,900,900i&display=swap', array(), null, 'all');

    //JS
    wp_enqueue_script('theme-js', get_template_directory_uri() . '/js/scripts.js', array(), null, false);
    wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.4.1.min.js', array(), null, false);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), null, false);
}
add_action('wp_enqueue_scripts', 'load_scripts');

/*
    ===================================
     Theme Menus
    ===================================
*/
function register_menus()
{
    register_nav_menu('main_menu', 'Menu Principal Lateral Esquerdo');
    register_nav_menu('footer_menu', 'Menu de Rodapé');
    register_nav_menu('social_icon_menu', 'Menu Redes Sociais');
}
add_action('init', 'register_menus');

/*
	==========================================
	 Type Unidade
	==========================================
*/
function enable_cadastro_unidade()
{

    $labels = array(
        'name' => 'Unidades',
        'singular_name' => 'Unidade',
        'add_new' => 'Adicionar Nova Unidade',
        'all_items' => 'Todas as Unidades',
        'add_new_item' => 'Adicionar Nova Unidade',
        'edit_item' => 'Editar Unidade',
        'new_item' => 'Adicionar Nova Unidade',
        'view_item' => 'Visualizar Unidade',
        'search_item' => 'Buscar Unidade',
        'not_found' => 'Nenhuma Unidade Encontrada',
        'not_found_in_trash' => 'Nenhuma Unidade na Lixeira',
        'parent_item_colon' => 'Unidade Pai'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'publicly_queryable' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'page',
        'hierarchical' => false,
        'supports' => array(
            'title',
            // 'editor',
            // 'excerpt',
            'thumbnail',
            'custom-fields',
            // 'page-attributes',
            'post-formats'
        ),
        'taxonomies' => array(),
        'menu_position' => 5,
        'exclude_from_search' => false
    );

    register_post_type('unidades', $args);
}
add_action('init', 'enable_cadastro_unidade');


/*
	==========================================
	 Type Instrutor
	==========================================
*/
function enable_cadastro_instrutor()
{

    $labels = array(
        'name' => 'Instrutor',
        'singular_name' => 'Instrutor',
        'add_new' => 'Adicionar Novo Instrutor',
        'all_items' => 'Todos os Instrutores',
        'add_new_item' => 'Adicionar Novo Instrutor',
        'edit_item' => 'Editar Instrutor',
        'new_item' => 'Adicionar Novo Instrutor',
        'view_item' => 'Visualizar Instrutor',
        'search_item' => 'Buscar Instrutor',
        'not_found' => 'Nenhum Instrutor Encontrado',
        'not_found_in_trash' => 'Nenhum Instrutor na Lixeira',
        'parent_item_colon' => 'Instrutor Pai'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'publicly_queryable' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'page',
        'hierarchical' => false,
        'supports' => array(
            'title',
            // 'editor',
            // 'excerpt',
            'thumbnail',
            'custom-fields',
            // 'page-attributes',
            'post-formats'
        ),
        'taxonomies' => array(),
        'menu_position' => 5,
        'exclude_from_search' => false
    );

    register_post_type('instrutor', $args);
}
add_action('init', 'enable_cadastro_instrutor');

/*
	==========================================
	 Widget area
	==========================================
*/

function ifly_widget_setup()
{
    register_sidebar(
        array(
            'name' => 'Instagram Feed',
            'id' => 'instagram_area',
            'class' => 'custom',
            'description' => 'Area destinada ao plugin do Instagram',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => ''
        )
    );

    register_sidebar(
        array(
            'name' => 'Tripadvisor Reviews',
            'id' => 'tripadvisor_area',
            'class' => 'custom',
            'description' => 'Area destinada ao plugin do Tripadvisor',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => ''
        )
    );
}

add_action('widgets_init', 'ifly_widget_setup');


/*
	==========================================
	 Theme Settings
	==========================================
*/
function display_theme_panel_fields()
{
    //Geral
    add_settings_section("section", "Opções Gerais", null, "theme-options");

    //URL Comprar
    add_settings_field("buy_tickets_url", "URL Botão Comprar", "display_buy_url_field", "theme-options", "section");
    register_setting("section", "buy_tickets_url");

    //URL Comprar no Menu
    add_settings_field("show_buy_menu", "Exibir Botão Comprar no Menu", "display_menu_buy", "theme-options", "section");
    register_setting("section", "show_buy_menu");

    //Carousel Timer
    add_settings_field("carousel_timer", "Tempo da Transição do Carrossel da Home", "display_carousel_timer_field", "theme-options", "section");
    register_setting("section", "carousel_timer");

    //Hashtah Campaign
    add_settings_field("hashtag_campaign", "Hashtag de Campanha", "display_input_hashtag", "theme-options", "section");
    register_setting("section", "hashtag_campaign");

    //Texto Pré Hashtag
    add_settings_field("text_before_hashtag", "Texto pré Hashtag", "display_input_pre_hashtag_text", "theme-options", "section");
    register_setting("section", "text_before_hashtag");
}

function display_input_hashtag()
{
    get_template_part('parts/settings', 'field-hashtag');
}

function display_input_pre_hashtag_text()
{
    get_template_part('parts/settings', 'field-pre-hashtag-text');
}

function display_carousel_timer_field()
{
    get_template_part('parts/settings', 'field-carousel-timer');
}

function display_buy_url_field()
{
    get_template_part('parts/settings', 'field-buy-url');
}

function display_menu_buy()
{
    get_template_part('parts/settings', 'field-menu-buy');
}

function theme_settings_page()
{
    get_template_part('parts/settings', 'page');
}

function add_theme_menu_item()
{
    add_menu_page("Configurações iFly", "Configurações iFly", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_init", "display_theme_panel_fields");
add_action("admin_menu", "add_theme_menu_item");


/*
	==========================================
	 Chortcodes
	==========================================
*/

function embed_lista_unidades()
{
    ob_start();
    get_template_part('parts/section', 'lista-unidades');
    return ob_get_clean();
}
add_shortcode('lista-unidades', 'embed_lista_unidades');