<svg xmlns="http://www.w3.org/2000/svg" width="63.009" height="40" viewBox="0 0 63.009 40">
    <g id="ifly-unidades" transform="translate(-650 -2085)">
        <path id="Caminho_1" data-name="Caminho 1"
            d="M0,14.543C21.875,25.8,26.907,36.933,30.355,38.32s12.8,2.259,15.693,1.228,7.926-5.31,10.937-11.294C40.856,40.658,32.733,21.677,63.009,0,49.614,6.261,34.516,19.3,28.057,26.353,25.481,25.956,12.364,18.744,0,14.543"
            transform="translate(650 2085)" />
        <path id="Caminho_2" data-name="Caminho 2"
            d="M60.668,34.867A5.993,5.993,0,1,1,54.684,29.2a5.841,5.841,0,0,1,5.984,5.667"
            transform="translate(620.599 2067.371)" />
    </g>
</svg>