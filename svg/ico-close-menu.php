<svg xmlns="http://www.w3.org/2000/svg" width="19.642" height="19.642" viewBox="0 0 19.642 19.642">
    <g id="Grupo_12" data-name="Grupo 12" transform="translate(-1090.679 299.488)">
        <path id="ic_menu_24px" d="M3,15.722H28V12.944H3Z" transform="translate(1079.405 -288.842) rotate(-45)" />
        <path id="ic_menu_24px-2" data-name="ic_menu_24px" d="M3,15.722H28V12.944H3Z"
            transform="translate(1099.675 -310.762) rotate(45)" />
    </g>
</svg>