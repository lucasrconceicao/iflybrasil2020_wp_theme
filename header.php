<!DOCTYPE html>
<html>

<head>
    <title>iFLY Brazil</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri() . '/' ?>favicon.png" />
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri() . '/' ?>favicon-apple.png">

    <?php wp_head(); ?>
</head>

<body>